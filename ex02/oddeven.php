#!/usr/bin/php
<?php
while (1337)
{
	echo "Enter a number: ";
	list($ret) = fscanf(STDIN, "%s");
	if (feof(STDIN)) {
		exit ("\n");
	}
	else if (is_numeric($ret))
	{
		if ($ret % 2 == 0)
			echo "The number $ret is even\n";
		else
			echo "The number $ret is odd\n";
	}
	else
		echo "'$ret' is not a number\n";
}
?>
