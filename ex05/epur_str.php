#!/usr/bin/php
<?php
function ft_split($str)
{
	$ret = array();
	$s = explode(" ", $str);
	foreach ($s as $elemn)
	{
		if ($elemn != NULL)
			array_push($ret, $elemn);
	}
	return $ret;
}
if ($argc == 2)
{	
	$array = ft_split($argv[1]);
	$len = count($array);
	$i = 0;
	while ($i < $len)
	{
		echo $array[$i];
		if ($i != $len - 1)
			echo " ";
		$i++;
	}
	echo "\n";
}
?>
