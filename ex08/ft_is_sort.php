#!/usr/bin/php
<?php
function ft_simple_sort($tab)
{
	$save = $tab;
	sort($tab);
	$len = count($save);
	$i = 0;
	while ($i < $len)
	{
		if (strcmp($tab[$i], $save[$i]) != 0)
			return false;
		$i++;
	}
	return true;
}

function ft_is_rsort($tab)
{
	$save = $tab;
	rsort($tab);
	$len = count($save);
	$i = 0;
	while ($i < $len)
	{
		if (strcmp($tab[$i], $save[$i]) != 0)
			return false;
		$i++;
	}
	return true;
}

function ft_is_sort($tab)
{
	if (ft_simple_sort($tab) || ft_is_rsort($tab))
		return true;
	return false;
}
?>
