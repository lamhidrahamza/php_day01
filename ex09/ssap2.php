#!/usr/bin/php
<?php
function ft_split($str)
{
	$ret = array();
	$s = explode(" ", $str);
	foreach ($s as $elemn)
	{
		if ($elemn != NULL)
			array_push($ret, $elemn);
	}
	return $ret;
}

function ft_compare($s1, $s2)
{
	$map = "abcdefghijklmnopqrstuvwxyz0123456789 !\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~";
	$s1 = strtolower($s1);
	$s2 = strtolower($s2);
	$len1 = strlen($s1);
	$len2 = strlen($s2);
	while ($i < $len1)
	{
		if ($i >= $len2)
			return 1;
		$pos1 = strpos($map, $s1[$i]);
		$pos2 = strpos($map, $s2[$i]);
		if ($pos1 < $pos2)
			return -1;
		else if ($pos1 > $pos2)
			return 1;
		$i++;
	}
	return 0;
}

if ($argc > 1)
{
	$i = 1;
	$result = array();
	while ($i < $argc)
	{
		$arr = ft_split($argv[$i]);
		$result = array_merge($result, $arr);
		$i++;
	}
	usort($result, ft_compare);
	foreach ($result as $elem)
		echo $elem."\n";
}
?>
