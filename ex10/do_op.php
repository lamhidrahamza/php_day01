#!/usr/bin/php
<?php

if ($argc == 4)
{
	$av1 = trim($argv[1]);
	$av2 = trim($argv[2]);
	$av3 = trim($argv[3]);
	if ($av2 == "+")
		echo $av1 + $av3;
	if ($av2 == "-")
		echo $av1 - $av3;
	if ($av2 == "*")
		echo $av1 * $av3;
	if ($av2 == "/")
		echo $av1 / $av3;
	if ($av2 == "%")
		echo $av1 % $av3;
	echo "\n";
}
else
	echo "Incorrect Parameters\n";

?>
