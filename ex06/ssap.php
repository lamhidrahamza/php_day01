#!/usr/bin/php
<?php
function ft_split($str)
{
	$ret = array();
	$s = explode(" ", $str);
	foreach ($s as $elemn)
	{
		if ($elemn != NULL)
			array_push($ret, $elemn);
	}
	return $ret;
}

if ($argc > 1)
{
	$i = 1;
	$result = array();
	while ($i < $argc)
	{
		$arr = ft_split($argv[$i]);
		$result = array_merge($result, $arr);
		$i++;
	}
	sort($result);
	foreach ($result as $elem)
		echo $elem."\n";
}
?>
