#!/usr/bin/php
<?php
function ft_split($str)
{
	$ret = array();
	$s = explode(" ", $str);
	foreach ($s as $elemn)
	{
		if ($elemn != NULL)
			array_push($ret, $elemn);
	}
	return $ret;
}

if ($argc > 1)
{
	$arr = ft_split($argv[1]);
	$len = count($arr);
	$i = 1;
	while ($i < $len)
	{
		echo $arr[$i]." ";
		$i++;
	}
	echo $arr[0];
	echo "\n";
}
?>
