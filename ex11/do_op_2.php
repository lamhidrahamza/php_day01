#!/usr/bin/php
<?php

if ($argc == 2)
{ 
	$error = 0;
	if (strpos($argv[1], '+'))
	{
		$array = explode('+', $argv[1]);
		$operator = "+";
	}
	if (strpos($argv[1], '-'))
	{	
		$array = explode('-', $argv[1]);
		$operator = "-";
	}
	if (strpos($argv[1], '*'))
	{
		$array = explode('*', $argv[1]);
		$operator = "*";
	}
	if (strpos($argv[1], '/'))
	{	
		$array = explode('/', $argv[1]);
		$operator = "/";
	}
	if (strpos($argv[1], '%'))
	{	
		$array = explode('%', $argv[1]);
		$operator = "%";
	}
	$av1 = trim($array[0]);
	$av2 = trim($array[1]);
	if (count($array) != 2 || !is_numeric($av1) || !is_numeric($av2))
		echo "Syntax Error";
	else
	{
		if ($operator == "+")
			echo $av1 + $av2;
		if ($operator == "-")
			echo $av1 - $av2;
		if ($operator == "*")
			echo $av1 * $av2;
		if ($operator == "/")
			echo $av1 / $av2;
		if ($operator == "%")
			echo $av1 % $av2;
	}
	echo "\n";
}
else
	echo "Incorrect Parameters\n";

?>
